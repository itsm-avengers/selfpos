import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { interval } from 'rxjs/Observable/interval';
import { map, distinctUntilChanged } from 'rxjs/operators'

/*
  Generated class for the NetStatusProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NetStatusProvider {
  networkStatus$: Observable<boolean> = interval(1000)
  .pipe(
    map(_ => navigator.onLine ),
    distinctUntilChanged()
  );

  constructor() {
    
  }

}
