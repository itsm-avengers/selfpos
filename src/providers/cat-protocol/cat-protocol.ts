// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CATIP } from '../../config';

import { Observable } from 'rxjs/Observable';
import { interval } from 'rxjs/Observable/interval';
import { fromPromise } from 'rxjs/Observable/fromPromise';
import { flatMap, distinctUntilChanged } from 'rxjs/operators'

/*
  Generated class for the CatProtocolProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

declare const SMTCAT;

@Injectable()
export class CatProtocolProvider {
  ip;
  connected = false;

  alive$: Observable<boolean> = interval(3000)
  .pipe(
    flatMap(_=>{
      return fromPromise(this.isConnected());
    }),
    distinctUntilChanged()
  );

  constructor() {
      
  }

  connect(){
    this.ip = localStorage.getItem('CATIP');

    return this.isConnected()
    .then(status=>{
      if (status)
        return this.disconnect();
      else
        return;
    })    
    .then(_=>{
      return new Promise((resolve, reject)=>{
        SMTCAT.connect(this.ip, 5555, _=>{
          if (_ == 'Connected') {
            this.connected = true;
            resolve();
          } else {
            reject();
          }
        }, _=>{
          console.error(_);
          reject();
        });
      });
    });
  }

  isConnected(): Promise<boolean> {
    return new Promise((resolve, reject)=>{
      SMTCAT.isConnected(_=>{
        _ = _=='true'?true:false;
        resolve(_);
      }, _=>{
        reject(_);
      });
    });
  }

  disconnect() {
    return new Promise((resolve, reject)=>{
      SMTCAT.disconnect(()=>{
        this.connected = false;
        resolve();
      }, err=>{
        resolve();
        console.error(err)
      });
    });
  }

  // type- 0: 신용, 1: 현금, 2: 은련
  // amt- 금액
  // instalment- 할부개월
  /* arr
  [
    {
      name: '상품명',
      amt: '가격',
      cnt: '수량'
    },
    ...
  ]
  */
  requestPay(type, amt, instalment, arr) {
    return new Promise((resolve, reject)=>{
      SMTCAT.trade(type, amt, instalment, arr, _=>{
        resolve(_);
      }, _=>{
        reject(_);
      });
    });
  }

  cancelTrade(type, amt, instalment, approvalNo, approvalDttm) {
    return new Promise((resolve, reject)=>{
      SMTCAT.tradeCancel(type, amt, instalment, approvalNo, approvalDttm, _=>{
        resolve(_);
      }, _=>{
        reject(_);
      });
    });
  }

  print(data) {
    return new Promise((resolve, reject)=>{
      SMTCAT.print(data, _=>{
        resolve(_);
      }, _=>{
        reject(_);
      });
    });
  }

  cancel() {
    return new Promise((resolve, reject)=>{
      SMTCAT.cancel(_=>{
        resolve(_);
      }, _=>{
        reject(_);
      });
    });
  }

  setOrderNo(no) {
    return new Promise((resolve, reject)=>{
      SMTCAT.setOrderNo(no, _=>{
        resolve(_);
      }, _=>{
        reject(_);
      });
    });
  }
}
