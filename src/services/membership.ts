import { Injectable } from "@angular/core";
import { HttpService } from './http.service';

@Injectable()
export class Membership {
    private resource: string = '/pay/pointpay';

    // public point: Point;
    public stampSet: StampSet;
    public stampAttr: StampAttr;
    // public mileage: Mileage;
    public notice: Notice;
    public push: Push;


    constructor(private httpService: HttpService) {
        // this.point = new Point(httpService);
        this.stampSet = new StampSet(httpService);
        this.stampAttr = new StampAttr(httpService);
        // this.mileage = new Mileage(httpService);
        this.push = new Push(httpService);
        this.notice = new Notice(httpService);

    }

    post(barcode, userNo, curPoint, payAmt, pointUse, pointSave, stampCnt, relNo = 0) {
        var params = {
            'barcode': barcode,
            'userNo': userNo,
            'curPoint': curPoint,//userPoint
            'payAmt': payAmt,
            'pointUse': pointUse,//usePoint 사용할 포인트
            'pointSave': pointSave, // 쓸 포인트
            'stampCnt': stampCnt,//적립할 스탬프 
            'relNo': relNo
        }
        return this.httpService.post(this.resource, params);
    }
}

export class StampSet {

    private resource: string = '/membership/stampSet';

    constructor(private httpService: HttpService) { }

    get() {
        return this.httpService.get('/membership/stampAll');
    }
    post(stampText, stampSavingAmt) {
        var params = {
            'stampText': stampText,
            'stampSavingAmt': stampSavingAmt,
        }
        return this.httpService.post(this.resource, params);
    }

}

export class StampAttr {

    private resource: string = '/membership/stampAttr';

    constructor(private httpService: HttpService) { }


    post(stampUnit, exchangeText) {
        var params = {
            'stampUnit': stampUnit,
            'exchangeText': exchangeText,
        }
        return this.httpService.post(this.resource, params);
    }

    put(stampUnit, exchangeText) {
        var params = {
            'stampUnit': stampUnit,
            'exchangeText': exchangeText,
        }
        return this.httpService.put(this.resource, params);
    }

    delete(stampUnit) {
        var params = {
            'stampUnit': stampUnit,
        }
        return this.httpService.delete(this.resource, params);
    }

}


// export class Point {

//     private resource: string = '/etc/point';

//     constructor(private httpService: HttpService) { }

//     get(startDt, endDt, pageNo = 1, pageSize = 10) {
//         var params = {
//             'startDt': startDt,
//             'endDt': endDt,
//             'pageNo': pageNo,
//             'pageSize': pageSize
//         }
//         return this.httpService.get(this.resource, params);
//     }

// }

// export class Stamp {

//     private resource: string = '/etc/stamp';

//     constructor(private httpService: HttpService) { }

//     get(startDt, endDt, pageNo = 1, pageSize = 10, tel = "") {
//         var params = {
//             'startDt': startDt,
//             'endDt': endDt,
//             'pageNo': pageNo,
//             'pageSize': pageSize,
//             'tel': tel + ''
//         }
//         return this.httpService.get(this.resource, params);
//     }

// }

// export class Mileage {

//     private resource: string = '/etc/mileage';

//     constructor(private httpService: HttpService) { }

//     get(startDt, endDt, pageNo = 1, pageSize = 10) {
//         var params = {
//             'startDt': startDt,
//             'endDt': endDt,
//             'pageNo': pageNo,
//             'pageSize': pageSize
//         }
//         return this.httpService.get(this.resource, params);
//     }

// }
export class Notice {
    private resource: string = '/membership/notice';

    constructor(private httpService: HttpService) { }

    get(search, areaNo, pageNo = 1, pageSize = 10) {
        var params = {
            search: search + '',
            areaNo: areaNo,
            pageNo: pageNo,
            pageSize: pageSize,
        };
        return this.httpService.get(this.resource, params);
    }
}
export class Push {
    private resource: string = '/membership/push';

    constructor(private httpService: HttpService) { }

    get(startDt, endDt, searchTxt, pageNo = 1, pageSize = 5) {
        var params = {
            startDt: startDt,
            endDt: endDt,
            searchTxt: searchTxt + '',
            pageNo: pageNo,
            pageSize: pageSize,
        };
        return this.httpService.get(this.resource, params);
    }
    post(pushTitle, pushTxt, publicParm, sendType, saveYN) {
        var params = {
            pushTitle: pushTitle,
            pushTxt: pushTxt,
            publicParm: publicParm,
            sendType: sendType,
            saveYN: saveYN,
        };
        return this.httpService.post(this.resource, params);
    }
}