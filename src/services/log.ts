import { Injectable } from "@angular/core";
import { HttpService } from './http.service';

@Injectable()
export class Log {
    public cardApproval: CardApproval;
    public cashPrint: CashPrint;
    public accountBook: AccountBook;
    public dailySales: DailySales;
    public monthlySales: MonthlySales;
    public menuSales: MenuSales;
    public clientUseStat: ClientUseStat;
    public storeUseStat: StoreUseStat;
    public salesPaytype: SalesPaytype;
    public salesMenu: SalesMenu

    constructor(private httpService: HttpService) {
        this.cardApproval = new CardApproval(this.httpService);
        this.cashPrint = new CashPrint(this.httpService);
        this.accountBook = new AccountBook(this.httpService);
        this.dailySales = new DailySales(this.httpService);
        this.monthlySales = new MonthlySales(this.httpService);
        this.menuSales = new MenuSales(this.httpService);
        this.clientUseStat = new ClientUseStat(this.httpService);
        this.storeUseStat = new StoreUseStat(this.httpService);
        this.salesPaytype = new SalesPaytype(this.httpService);
        this.salesMenu = new SalesMenu(this.httpService);
        
    }
}

export class CardApproval {

    private resource: string = '/record/cardApproval';

    constructor(private httpService: HttpService) { }

    get(startDt, endDt, pageNo = 1, pageSize = 20) {
        var params = {
            'startDt': startDt,
            'endDt': endDt,
            'pageNo': pageNo,
            'pageSize': pageSize
        }
        return this.httpService.get(this.resource, params);
    }

}

export class CashPrint {

    private resource: string = '/record/cashPrint';

    constructor(private httpService: HttpService) { }

    get(startDt, endDt, pageNo = 1, pageSize = 20) {
        var params = {
            'startDt': startDt,
            'endDt': endDt,
            'pageNo': pageNo,
            'pageSize': pageSize
        }
        return this.httpService.get(this.resource, params);
    }

}

export class AccountBook {

    private resource: string = '/record/accountBook';

    constructor(private httpService: HttpService) { }

    get(startDt, endDt) {
        var params = {
            'startDt': startDt,
            'endDt': endDt
        }
        return this.httpService.get(this.resource, params);
    }

}

export class DailySales {

    private resource: string = '/record/dailySales';

    constructor(private httpService: HttpService) { }

    get(dt, pageNo = 1, pageSize = 20) {
        var params = {
            'dt': dt,
            'pageNo': pageNo,
            'pageSize': pageSize
        }
        return this.httpService.get(this.resource, params);
    }

}

export class MonthlySales {

    private resource: string = '/record/monthlySales';

    constructor(private httpService: HttpService) { }

    get(chooseYear, pageNo = 1, pageSize = 20) {
        var params = {
            'chooseYear': chooseYear,
            'pageNo': pageNo,
            'pageSize': pageSize
        }
        return this.httpService.get(this.resource, params);
    }

}

export class MenuSales {

    private resource: string = '/record/menuSales';

    constructor(private httpService: HttpService) { }

    get(startDt, endDt, pageNo = 1, pageSize = 20) {
        var params = {
            'startDt': startDt,
            'endDt': endDt,
            'pageNo': pageNo,
            'pageSize': pageSize
        }
        return this.httpService.get(this.resource, params);
    }

}

export class ClientUseStat {

    private resource: string = '/record/clientUseStat';

    constructor(private httpService: HttpService) { }

    get() {
        return this.httpService.get(this.resource);
    }

}

export class StoreUseStat {

    private resource: string = '/record/storeUseStat';

    constructor(private httpService: HttpService) { }

    get() {
        return this.httpService.get(this.resource);
    }

}

export class SalesPaytype {

    private resource: string = '/record/salesPaytype';

    constructor(private httpService: HttpService) { }

    get(startDt, endDt, filter = 0, pageNo = 1, pageSize = 20) {
        var params = {
            'startDt': startDt,
            'endDt': endDt,
            'filter': filter,
            'pageNo': pageNo,
            'pageSize': pageSize
        }
        return this.httpService.get(this.resource, params);
    }

}

export class SalesMenu {

    private resource: string = '/record/salesMenu';

    constructor(private httpService: HttpService) { }

    get(startDt, endDt,filter = 0) {
        var params = {
            'startDt': startDt,
            'endDt': endDt,
            'filter':filter
        }
        return this.httpService.get(this.resource, params);
    }

}

