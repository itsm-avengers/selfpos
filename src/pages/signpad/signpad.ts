import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

declare const SignaturePad;

/**
 * Generated class for the SignpadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signpad',
  templateUrl: 'signpad.html',
})
export class SignpadPage implements OnInit, AfterViewInit {
  
  @ViewChild('canvas') canvasRef: ElementRef;
  private canvas: HTMLCanvasElement;
  signaturePad;

  ngOnInit(): void {
    this.canvas = this.canvasRef.nativeElement;
    this.canvas.width = this.canvas.clientWidth;
    this.canvas.height = this.canvas.clientWidth * 0.6;
  }

  ngAfterViewInit(): void {
    // https://github.com/szimek/signature_pad
    this.signaturePad = new SignaturePad(this.canvas, {
      minWidth: 1,
      maxWidth: 5
    });
  }

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignpadPage');

  }

}
