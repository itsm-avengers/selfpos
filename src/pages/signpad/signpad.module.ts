import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignpadPage } from './signpad';

@NgModule({
  declarations: [
    SignpadPage,
  ],
  imports: [
    IonicPageModule.forChild(SignpadPage)
  ],
})
export class SignpadPageModule {}
