import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams, ViewController, Content } from 'ionic-angular';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  @ViewChild(Content) content: Content;
  category;
  menu;
  opts = [];

  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
    this.category = navParams.get('category');
    if (!this.category)
      this.menu = navParams.get('menu');
  }

  ionViewDidLoad() {
  }

  selectItem(menu, select = false) {
    if (!select && (menu.opts.length || menu.optGroups.length)) {
      this.content.resize();
      this.menu = menu;
    } else {
      this.opts.sort((a, b)=>{
        return +a.id - +b.id;
      });
      this.viewCtrl.dismiss({ menu: menu, opts: this.opts });
    }
  }

  toggleOption(opt, group = null) {
    let i = this.opts.findIndex(_=>_.id == opt.id);
    if (i >= 0) {
      this.opts.splice(i, 1);
    } else {
      if (group && group.multYn != '1') {
        group.opts.forEach(opt => {
          let i = this.opts.findIndex(_=>_.id == opt.id);
          if (i >= 0)
            this.opts.splice(i, 1);
        });
      }
      this.opts.push(opt);
    }
  }

  isActive(opt) {
    return !!this.opts.find(_=>_.id == opt.id)
  }

  total() {
    return this.menu.saleAmt + this.opts.reduce((__, _)=>__+_.amt, 0);
  }
}
