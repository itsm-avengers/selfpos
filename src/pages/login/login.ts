import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';

import { CatProtocolProvider } from '../../providers/cat-protocol/cat-protocol';
import { CATIP } from '../../config';
import { HttpService } from '../../services/http.service';
import { Auth } from '../../services/auth';
import { LoginSession } from '../../services/login.session';
import { Sales } from '../../services/sales';
import { map } from 'rxjs/operators';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  toast;
  status;
  ip;
  bixolonIp;

  id;
  pw;
  idSaveCheck;
  isCafe = false;
  useTcp = false;

  constructor(
    public navCtrl: NavController, public navParams: NavParams, 
    private cat: CatProtocolProvider, private toastCtrl: ToastController, 
    private storage: NativeStorage, private http: HttpService, 
    private auth: Auth, public loginSession: LoginSession,
    private sales: Sales
  ) {
    let tmp = localStorage.getItem('CATIP');
    if (tmp && tmp != 'undefined')
      this.ip = tmp;
    else
      this.ip = CATIP;
    
    tmp = localStorage.getItem('BIXOLONIP');
    if (tmp && tmp != 'undefined') {
      this.bixolonIp = tmp;
      this.useTcp = true;
    }

    tmp = localStorage.getItem('CAFE');
    if (tmp && tmp != 'undefined')
      this.isCafe = true;
  }

  ionViewDidLoad() {    
    this.storage.getItem('loginId')
    .then(
      (data) => {
        this.id = data.id;
        this.idSaveCheck = true;
        //this.login(); //테스트
      },
      error => {
      }
    );
  }

  setUseTcp(checked) {
    if (!checked) {
      localStorage.removeItem('BIXOLONIP');
    }
  }

  login() {
    if (!this.id || !this.pw) {
      alert('아이디/비밀번호를 입력하세요.');
      return;
    }

    this.http.oauth.login(this.id, this.pw)
    .subscribe(
      data => {
        this.http.oauth.setToken(data.json().access_token, data.json().refresh_token, data.json().expires_in);
        if (this.idSaveCheck)
          this.saveId();
        else
          this.clearSavedId();

        this.auth.getInfo()
        .pipe(map(data => data.json()))
        .subscribe(
          (data: any) => {
            data.pwd = this.pw;
            this.loginSession.setInfo(data);
            localStorage.setItem('token', data.storeNo);
            this.sales.end.getNo()
              .subscribe(
                async data => {
                  this.showToast('로그인에 성공했습니다.');
                  
                  localStorage.setItem('CATIP', this.ip);
                  if (this.isCafe) {
                    localStorage.setItem('CAFE', 'true');
                  } else {
                    localStorage.removeItem('CAFE');
                  }
                  
                  let no = localStorage.getItem('ORDER_NO');
                  await this.cat.setOrderNo(no?+no:0);
                  this.navCtrl.setRoot('OrderPage', {
                    cafe: this.isCafe
                  });
                },
                err => {
                  // 개시필요
                  alert('영업마감 상태입니다. 영업개시 후 다시 로그인 해 주세요.');
                }
              );
          }
        );
      },
      err => {
        if (err == "network")
          this.showToast('인터넷 연결을 확인해 주세요.');
        else{
          this.showToast('아이디 혹은  비밀번호를 확인해 주세요.');
        console.log('login error: ' + JSON.stringify(err.json()));
        }
      }
    );
  }

  setIp() {
    let ip = prompt('결제단말 IP', this.ip);
    if (ip) {
      this.ip = ip;
    }
  }

  setPrnterIp() {
    let ip = prompt('프린터 IP', this.bixolonIp);
    if (ip) {
      this.bixolonIp = ip;
      localStorage.setItem('BIXOLONIP', ip);
    }
  }

  async saveId() {
    await this.storage.setItem('loginId', { id: this.id });
  }

  async clearSavedId() {
    await this.storage.remove('loginId');
  }

  showToast(text) {
    this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    }).present();
  }

  async setOrderNo() {
    let no = prompt('시작 주문번호', localStorage.getItem('ORDER_NO'));
    if (no) {
      await this.cat.setOrderNo(+no);
      localStorage.setItem('ORDER_NO', no);
    }
  }
}