import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

/**
 * Generated class for the RepairPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-repair',
  templateUrl: 'repair.html',
})
export class RepairPage {
  startDttm;
  endDttm;
  message;

  constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController) {
    this.message = navParams.get('message');
    this.startDttm = navParams.get('startDttm');
    this.endDttm = navParams.get('endDttm');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RepairPage');
  }

  refresh() {
    this.loadingCtrl.create().present();
    location.reload();
  }
}
