import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { CatProtocolProvider } from '../../providers/cat-protocol/cat-protocol';
import { Pay } from '../../services/pay';
import * as moment from 'moment';
import { gql, Apollo } from 'apollo-angular-boost';

/**
 * Generated class for the PayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

const AddOrder = gql`
mutation AddOrder($input: OrderInput!, $details: [OrderDetailInput], $deviceId: String) {
  addOrder(input: $input, details: $details, deviceId: $deviceId) {
    id
    total
    detail {
      id
      menuNo
      menuName
      diff
      itemCNT
      itemAmt
      stockCNT
      taxType
      dcAmt
      dcName
    }
    error {
      key
      value
    }
  }
}
`;

 declare const BIXOLON;

@IonicPage()
@Component({
  selector: 'page-pay',
  templateUrl: 'pay.html'
})
export class PayPage {
  title = `포장할까요?<br />Take out?`;;
  order = [];
  total = 0;
  message;
  err;
  mode = 'select';
  invert = false;

  timeout;

  res;

  takeaway = false;

  constructor(
    public navCtrl: NavController, public navParams: NavParams, 
    public viewCtrl: ViewController, private cat: CatProtocolProvider,
    private pay: Pay, private apollo: Apollo
  ) {
  }

  ionViewDidLoad() {
    this.order = this.navParams.get('order');
    this.total = this.navParams.get('total');
  }

  selectMode(mode) {
    this.mode = mode;
    if (mode == 'card') {
      let timeout;
      this.connect()
      .then(_=>{
        return new Promise((resolve, reject)=>{
          BIXOLON.getStatus(_=>{
            clearTimeout(timeout);
            resolve();
          }, reject);
          timeout = setTimeout(_=>{
            reject('주방프린터 연결안됨');
            BIXOLON.disconnect();
          }, 3000);
        });
      })
      .then(_=>{
        return this.apollo.mutate({
          mutation: AddOrder,
          variables: {
            input: {
              orderNo: null,
              orderName: '-',
              orderType: '1',
              floorNo: -1,
              waitingNo: -1,
              tables: ''
            },
            details: this.order.map(_=>{
              return {
                menuNo: _.menu.menuNo,
                menuName: _.menu.name.ko,
                diff: _.cnt * -1,
                itemCNT: _.cnt,
                itemAmt: _.menu.saleAmt,
                taxType: _.menu.taxType,
                dcAmt: 0,
                dcName: '',
                opts: _.opts.map(_=>{
                  return {
                    menu_opt_no: _.id
                  }
                })
              };
            })
          }
        }).toPromise();
      })
      .then( (order: any) => {
        this.title = '카드결제<br />Card Payment';
        this.message = 'IC카드를 넣어주세요.<br />Insert your card.';
        return this.cat.requestPay(0, this.total, 0, this.order)
        .then(res=>{
          return this.payCard(res, order.data.addOrder.id)
          .then(_=>res);
        })
      })
      .then(
        (res: any) => {
          this.mode = 'card_done';
          this.title = '결제완료<br />Complete Transaction';
          this.message = 'IC카드를 빼주세요.<br />Please remove the card';
          this.invert = true;
          this.res = res;
          this.timeout = setTimeout(_=>{
            this.close();
          }, 10000);

          // setTimeout(() => {
          //   this.cat.print([
          //     `- 주문번호 -`,
          //     `                  ${res.orderNo}`
          //   ]);
          // }, 1000);
          
          BIXOLON.print(
            `주문번호: ${res.orderNo} ${this.takeaway?'(포장)':''}\n`,
            this.order
          );
        }
      )
      .catch(err=>{
        this.mode = 'card_error';
        this.title = '결제실패<br />Incomplete Transaction';
        this.message = '오류가 발생했습니다.<br />An error has occurred';
        this.invert = true;
        this.err = err;
        this.res = { success: false };
      })
    }
  }

  async connect() {
    // 결제단말기 연결
    await Promise.all([
      this.cat.connect(),
      new Promise((resolve, reject)=>{
        BIXOLON.connect(localStorage.getItem("BIXOLONIP"), null, ()=>{
          console.log('BIXOLON connected');
          resolve();
        }, (e)=>{
          console.error(e);
          reject();
        });
      })
    ]);
  }

  async close() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    
    if (this.res) {
      this.viewCtrl.dismiss({ res: true });
    } else {
      if (this.mode == 'card') {
        await this.cat.cancel();
      }
      this.viewCtrl.dismiss({ res: false });
    }
  }

  payCard(res, orderNo) {
    
    //카드 쓰기 
    return this.pay.postCard(
      orderNo,
      this.total,//총 주문 금액 
      this.total, //cardAmt
      0,//mileageAmt
      0,//pointAmt
      res.cardID,
      '',
      res.issueName,
      '',
      0,
      '',
      this.total,
      res.approvalNo,
      res.approvalDate,
      '',
      '',
      '',
      '',//barcode
      0,//userNo
      0,//curPoint
      '2'
    ).toPromise()
    .then(_=>res);
  }
}
