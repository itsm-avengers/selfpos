import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, Slides, Modal } from 'ionic-angular';
import { Apollo, gql, QueryRef } from 'apollo-angular-boost';
import { HttpClient } from '@angular/common/http';
import { trigger,style,transition,animate,keyframes,query,stagger } from '@angular/animations';

import { map, bufferTime, filter } from 'rxjs/operators';
import { Observable, fromEvent, merge, Subscription } from 'rxjs';
import { CatProtocolProvider } from '../../providers/cat-protocol/cat-protocol';

declare const BIXOLON;

const StoreQuery = gql`
  query GetStore {
    getStore {
      id
      posNm
      posBackUrl
      promotionImg
      category {
        id
        name {
          ko
        }
        menu {
          id
          menuNo
          name {
            ko
          }
          categoryNo
          saleAmt
          stampUseYN
          takeoutDCAmt
          menuBgColor
          dispOrder
          menuType
          stockCNT
          stockSaleYN
          soldOutYN
          thumbnailS
          unitPrice
          menuCode
          startDt
          endDt
          taxType
          useYN
          opts {
            id
            name
            amt
          }
          optGroups {
            id
            multYn
            name
            opts {
              id
              name
              amt
            }
          }
        }
      }
    }
  }
`;

const IDLE_TIME = 50000;
const RESET_TIME = 10000;
const POLL_INTERVAL = 60000;

@IonicPage()
@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
  animations: [
    trigger('addOrder', [
      transition(':enter', [
        animate('.4s ease-in', keyframes([
          style({background: '#fff', opacity: 0, transform: 'translateX(100%) rotateZ(10deg)', offset: 0}),
          style({background: '#fafafa', opacity: .5, transform: 'translateX(20%) rotateZ(-5deg)',  offset: 0.3}),
          style({background: '#dadada', opacity: 1, transform: 'translateX(0%) rotateZ(0)',     offset: 1.0}),
        ]))]),
      transition(':leave', [
        animate('.4s ease-out', keyframes([
          style({background: '#dadada', opacity: 1, transform: 'translateX(0%)', offset: 0}),
          style({background: '#fafafa', opacity: .5, transform: 'translateX(20%)',  offset: 0.3}),
          style({background: '#fff', opacity: 0, transform: 'translateX(100%)',     offset: 1.0}),
        ]))])
    ]),
    trigger('changeQuantity', [
      transition('* => *', [
        animate('.6s ease-in', keyframes([
          style({background: '#dadada', opacity: 1, transform: 'rotateZ(10deg)', offset: 0}),
          style({background: '#fafafa', opacity: .5, transform: 'rotateZ(-10deg)',  offset: 0.5}),
          style({background: '#fff', opacity: 1, transform: 'rotateZ(0)',     offset: 1.0}),
        ]))])
    ])
  ]
})
export class OrderPage {
  @ViewChild(Slides) slides: Slides;
  
  title;
  data;
  order = [];
  query: QueryRef<any>;

  pages;
  page = 0;

  isCafe = false;

  ev: Observable<any> = merge(
    fromEvent(document, 'click'),
    fromEvent(document, 'keydown'),
    fromEvent(document, 'mousemove'),
    fromEvent(document, 'touchstart'),
    fromEvent(document, 'scroll')
  );
  idleSub: Subscription;
  resetTimer;
  
  modal: Modal;
  payModal: Modal;
  bShowIntro = false;

  category;

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    private modalCtrl: ModalController, private apollo: Apollo,
    private toastCtrl: ToastController, private cat: CatProtocolProvider,
    private http: HttpClient
  ) {
    
  }

  ionViewDidLoad() {
    this.isCafe = this.navParams.get('cafe');
    this.query = this.apollo
      .watchQuery({
        query: StoreQuery,
        fetchPolicy: "network-only",
        pollInterval: POLL_INTERVAL
      });
    this.query
    .valueChanges.pipe(map((_: any)=>_.data && _.data.getStore))
    .subscribe((result: any) => {
      this.data = result;
      if (this.isCafe) {
        this.http.get(`https://s3.ap-northeast-2.amazonaws.com/hgus3/menu/${result.id}.json`)
        .subscribe((_: any)=>{
          this.pages = _;
        });
      }
      this.showIntro();
    });
  }

  ionViewWillLeave() {
    if (this.modal)
      this.modal.dismiss()
      .then(_=>{
        if (this.idleSub)
          this.idleSub.unsubscribe();
      });
    if (this.payModal)
      this.payModal.dismiss();
    if (this.idleSub)
      this.idleSub.unsubscribe();
    if (this.resetTimer)
      clearTimeout(this.resetTimer);
  }

  showMenuOption(item, showCategory = false) {
    let param: any = {};
    if (showCategory) {
      param.category = this.data.category.find(_=>_.id == item.category)
    } else {
      param.menu = item;
    }
    let modal = this.modalCtrl.create('MenuPage', param, {
      cssClass: 'menu-modal'
    });

    modal.onDidDismiss(data=>{
      this.subscribeIdleChecker();
      if (data && data.menu) {
        this.addOrder(data.menu, data.opts);
      }
    });

    if (this.idleSub)
      this.idleSub.unsubscribe();
    modal.present();
  }

  paging(n) {
    this.page += n;
    if (this.page < 0) {
      this.page = 0;
    } else {
      let totalPage = Math.ceil(this.count() / 12.0) - 1;
      if (this.page > totalPage)
        this.page = totalPage;
    }
  }

  count(): number {
    if (this.category == null) {
      return this.data.category.reduce((prev, cur)=>{
        return prev + cur.menu.length;
      }, 0);
    } else {
      return this.data.category[this.category].menu.length;
    }
  }

  showIntro() {
    this.bShowIntro = true;
    if (this.modal)
      return;

    this.modal = this.modalCtrl.create('IntroPage', { img: this.data.promotionImg }, { cssClass: 'intro', enableBackdropDismiss: false });
    this.modal.onDidDismiss(_=>{
      // this.connect();
      this.modal = null;
      this.bShowIntro = false;
      this.subscribeIdleChecker();
    })
    this.modal.present();
  }

  subscribeIdleChecker() {
    if (this.idleSub)
      this.idleSub.unsubscribe();

    this.idleSub = this.ev.pipe(
      bufferTime(IDLE_TIME),
      filter((arr: any) => {
        return arr.length === 0;
      })
    ).subscribe(_=>{
      this.idleSub.unsubscribe();
      this.triggerResetTimer();
    });
  }

  triggerResetTimer() {
    if (this.modal)
      return;
      
    let toast = this.toastCtrl.create({
      message: `${RESET_TIME / 1000}초 동안 입력이 없으면 주문이 취소됩니다..`,
      duration: RESET_TIME
    });
    toast.present();
    this.resetTimer = setTimeout(_=>{
      this.reset();
    }, RESET_TIME);
    let subscription = this.ev.subscribe(_=>{
      subscription.unsubscribe();
      toast.dismiss();
      clearTimeout(this.resetTimer);
      this.subscribeIdleChecker();
    });

  }

  price(order) {
    return (order.menu.saleAmt + order.opts.reduce((__, _)=>__+_.amt, 0)) * order.cnt;
  }

  total() {
    return this.order.reduce((prev,cur)=>{
      return prev + this.price(cur);
    }, 0);
  }

  selectAll() {
    this.order.forEach(_=>_.selected = true);
  }

  deleteSelected() {
    this.order = this.order.filter(_=>!_.selected);
  }

  addOrder(menu, opts = []) {
    if (menu.soldOutYN != '0')
      return;
    
    let item;
    if (item = this.order.find(_=>{
      return menu.id == _.menu.id && opts.length == _.opts.length && opts.reduce((prev, cur, index)=>{
        return prev && cur.id == _.opts[index].id;
      }, true);
    })) {
      item.cnt++;
    } else {
      this.order.unshift({
        cnt: 1,
        menu: menu,
        opts: opts
      });
    }
  }

  add(item) {
    item.cnt++;
  }

  remove(item) {
    if (item.cnt > 1)
      item.cnt--;
    else
      this.order.splice(this.order.findIndex(_=>_==item), 1);
  }

  pay() {
    if (this.order.length) {
      this.payModal = this.modalCtrl.create('PayPage', { 
        order: this.order, 
        total: this.total()
      }, {
        enableBackdropDismiss: false
      });

      this.payModal.onDidDismiss(data=>{
        this.cat.disconnect();
        BIXOLON.disconnect();
        
        this.subscribeIdleChecker();
        if (data && data.res) {
          this.reset();
        }
      });

      if (this.idleSub)
        this.idleSub.unsubscribe();
      this.payModal.present();
    }
  }

  reset() {
    this.order = [];
    this.query.refetch();
    this.showIntro();
  }
}
