
export const HOST = `https://192.168.127.0:9444`;//실서버
export const API = `${HOST}/pcp`;
export const GRAPHQL_ENDPOINT = `${HOST}/graphql`;
export const PACKAGE_NAME = `kr.go.pohang.sarang.kiosk`;
export const CATIP = `192.168.10.10`;
