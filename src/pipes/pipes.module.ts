import { NgModule } from '@angular/core';
import { MenuFlattenPipe } from './menu-flatten/menu-flatten';
import { SlicePipe } from './slice/slice';
import { PagerPipe } from './pager/pager';
@NgModule({
	declarations: [
		MenuFlattenPipe,
		SlicePipe,
    PagerPipe
	],
	imports: [],
	exports: [
		MenuFlattenPipe,
		SlicePipe,
    PagerPipe
	]
})
export class PipesModule {}
