import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the SplicePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'slice',
})
export class SlicePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: any[], start: number = 0, count: number = 1) {
    let arr = value.slice(start, start + count);
    return arr.concat(Array(12 - arr.length).fill(null));
  }
}
