import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the PagerPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'pager',
})
export class PagerPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: number, nItem: number) {
    return Array(Math.ceil(value / nItem))
      .fill("")
      .map((v, i) => i);
  }
}
