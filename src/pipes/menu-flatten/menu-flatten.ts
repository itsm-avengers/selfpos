import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the MenuFlattenPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'menuFlatten',
})
export class MenuFlattenPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: any[], ...args) {
    const index = args[0];

    if (index == null)
      return value.reduce((prev, cur)=>{
        prev.push(...cur.menu);
        return prev;
      }, []);
    else
      return value[index].menu;
  }
}
