import { NgModule } from '@angular/core';
import { ImageMapComponent } from './image-map/image-map';
import { CommonModule } from '@angular/common';

@NgModule({
	declarations: [ImageMapComponent],
	imports: [CommonModule],
	exports: [ImageMapComponent]
})
export class ComponentsModule {}
