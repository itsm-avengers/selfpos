import {
  Component, ElementRef, EventEmitter, Input, Output, ViewChild, OnInit, OnDestroy, AfterViewInit
} from '@angular/core';

import * as _ from 'lodash';

export interface Map {
  coord: string
  category: number
}

@Component({
  selector: 'img-map',
  template: `
  <img #page [src]="src" usemap="#page">
  <map name="page">
    <area *ngFor="let item of maps" shape="rect" [attr.coords]="item.coord" (click)="OnClick.emit(item)" target="target">
  </map>
  `,
  styles: [
  `
  img {
    position: fixed;
    top: 0;
    left: 0;
    z-index: 0;
    width: 100%;
  }
  `
  ]
})
export class ImageMapComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('page') image: ElementRef;
  @Input('maps')
  set setMarkers(maps) {
    this._maps = _.cloneDeep(maps);
    if (this.resizeMap)
      this.resizeMap();
  }

  maps: Map[] = [];
  _maps: Map[] = [];

  /**
   * Image source URL.
   */
  @Input()
  src: string;

  /**
   * On mark event.
   */
  @Output('OnClick')
  OnClick = new EventEmitter<any>();

  timer;

  constructor() {
    
  }

  ngOnInit(): void {
    
  }

  ngAfterViewInit(): void {
    const resizeMap = this.resizeMap = () =>{
      let scalingFactor = {
        width  : this.image.nativeElement.width  / this.image.nativeElement.naturalWidth,
        height : this.image.nativeElement.height / this.image.nativeElement.naturalHeight
      };
  
      let padding = {
        width  : parseInt(window.getComputedStyle(this.image.nativeElement, null).getPropertyValue('padding-left'), 10),
        height : parseInt(window.getComputedStyle(this.image.nativeElement, null).getPropertyValue('padding-top'), 10)
      };
  
      let isWidth = 0;
      
      this.maps = _.cloneDeep(this._maps);
      this.maps.map(map=>{
        map.coord = map.coord.split(',').map(coord=>{
          let dimension = ( 1 === (isWidth = 1-isWidth) ? 'width' : 'height' );
          return padding[dimension] + Math.floor(Number(coord) * scalingFactor[dimension]);
        }).join(',');
      });
    };

    const debounce = this.debounce = () => {
      clearTimeout(this.timer);
      this.timer = setTimeout(this.resizeMap, 250);
    }

    this.image.nativeElement.addEventListener('load',  resizeMap, false); //Detect late image loads in IE11
    window.addEventListener('focus',  resizeMap, false); //Cope with window being resized whilst on another tab
    window.addEventListener('resize', debounce,  false);
    window.addEventListener('readystatechange', resizeMap,  false);
    document.addEventListener('fullscreenchange', resizeMap,  false);
  }
  
  ngOnDestroy(): void {
    this.image.nativeElement.removeEventListener('load',  this.resizeMap, false); //Detect late image loads in IE11
    window.removeEventListener('focus',  this.resizeMap, false); //Cope with window being resized whilst on another tab
    window.removeEventListener('resize', this.debounce,  false);
    window.removeEventListener('readystatechange', this.resizeMap,  false);
    document.removeEventListener('fullscreenchange', this.resizeMap,  false);
  }

  resizeMap;
  debounce;
}