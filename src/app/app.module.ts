import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorHandler, NgModule } from '@angular/core';

import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { NativeStorage } from '@ionic-native/native-storage';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';

import { MyApp } from './app.component';
import { NetStatusProvider } from '../providers/net-status/net-status';
import { CatProtocolProvider } from '../providers/cat-protocol/cat-protocol';
import { HttpService } from '../services/http.service';
import { Auth } from '../services/auth';
import { LoginSession } from '../services/login.session';
import { Sales } from '../services/sales';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpHeaders } from '@angular/common/http';

import { ApolloBoostModule, Apollo, InMemoryCache, ApolloLink } from "apollo-angular-boost";
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { GRAPHQL_ENDPOINT } from '../config';
import { Pay } from '../services/pay';
import { Device } from '@ionic-native/device';
import { AppUpdate } from '@ionic-native/app-update';
import { Firebase } from '@ionic-native/firebase';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    ApolloBoostModule,
    HttpLinkModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AndroidFullScreen,
    NativeStorage,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    NetStatusProvider,
    CatProtocolProvider,
    HttpService,
    Auth,
    LoginSession,
    Sales,
    Device,
    AppUpdate,
    Firebase,
    Pay
  ]
})
export class AppModule {
  constructor(apollo: Apollo, httpLink: HttpLink, httpService: HttpService) {
    const authMiddleware = new ApolloLink((operation, forward) => {
      let token = localStorage.getItem('access_token');
      token = token ? `Bearer ${token}` : null
      

      // add the authorization to the headers
      operation.setContext({
        headers: new HttpHeaders().set('Authorization', token)
      });

      return forward(operation);
    });

    apollo.create({
      link: authMiddleware.concat(httpLink.create(({
        uri: GRAPHQL_ENDPOINT
      }))),
      cache: new InMemoryCache()
    });
  }
}
