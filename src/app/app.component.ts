import { Component, ViewChild } from '@angular/core';
import { Platform, ToastController, ModalController, Modal, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AppUpdate } from '@ionic-native/app-update';
import { Firebase } from '@ionic-native/firebase';

import { NetStatusProvider } from '../providers/net-status/net-status';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';
import { HttpClient } from '@angular/common/http';

export enum STATUS {
  REPAIR = "REPAIR"
}

export enum TYPE {
  ORDER_CHANGED = 'ORDER_CHANGED',
  ORDER_DELETED = 'ORDER_DELETED',
  PAID = 'PAID',
  IMMEDIATE_REPAIR_START = 'IMMEDIATE_REPAIR_START',
  IMMEDIATE_REPAIR_END = 'IMMEDIATE_REPAIR_END',
  UPDATE_REQUIRED = 'UPDATE_REQUIRED',
  MENU_REFRESHED = 'MENU_REFRESHED',
  APP_LOGOUT = 'APP_LOGOUT',
  CAT_CANCEL_PAY = 'CAT_CANCEL_PAY'
}

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = 'LoginPage';
  repairModal: Modal;

  constructor(
    platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, 
    toastCtrl: ToastController, netStat: NetStatusProvider, androidFullScreen: AndroidFullScreen,
    private appUpdate: AppUpdate, fcm: Firebase, http: HttpClient, private modalCtrl: ModalController
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      androidFullScreen.immersiveMode();

      // Check for immediate repair
      http.get(`https://s3.ap-northeast-2.amazonaws.com/hgus3/common/status.json`)
      .subscribe((_: any)=>{
        switch (_.SELFPOS) {
          case STATUS.REPAIR:
            http.get(`https://s3.ap-northeast-2.amazonaws.com/hgus3/common/repair.json`)
            .subscribe((_: any)=>{
              this.immediateRepair(_)
            });
          break;
        }
      });

      // Check for update
      this.checkUpdate();
      
      // Register FCM callback
      fcm.subscribe("all");
      // fcm.subscribe("dev");
      fcm.onNotificationOpen()
      .subscribe(msg=>{
        console.log(msg);
        if (msg.payload)
          msg.payload = JSON.parse(msg.payload);
        switch (msg.type) {
          case TYPE.IMMEDIATE_REPAIR_START:
            this.immediateRepair(msg.payload);
          break;
          case TYPE.IMMEDIATE_REPAIR_END:
            if (this.repairModal)
              this.repairModal.dismiss();
          break;
          case TYPE.UPDATE_REQUIRED:
          if (msg.payload.apps.includes('SELFPOS'))
              this.checkUpdate();
          break;
          case TYPE.APP_LOGOUT:
          this.nav.setRoot(this.rootPage);
          break;
          case TYPE.CAT_CANCEL_PAY:

          break;
        }
      });

      fcm.onTokenRefresh()
      .subscribe(_=>{
        console.log(_);
      });
      
      let toast;
      netStat.networkStatus$.subscribe(status=>{
        if (!status) {
          toast = toastCtrl.create({
            message: '경고! 네트워크 연결안됨',
            position: 'top',
            cssClass: 'toast alert'
          });
          toast.present();
        } else {
          if (toast) {
            toast.dismiss();
            toast = null;
          }
        }
      });
    });
  }

  checkUpdate() {
    this.appUpdate.checkAppUpdate(`https://s3.ap-northeast-2.amazonaws.com/hgus3/version/SELFPOS.xml`)
    .then(_=>console.log('Update available'));
  }

  immediateRepair(data) {
    if (this.repairModal || !data.apps.includes('SELFPOS'))
      return;
      
    this.repairModal = this.modalCtrl.create('RepairPage', data, { enableBackdropDismiss: false });
    this.repairModal.onDidDismiss(_=>this.repairModal = null);
    this.repairModal.present();
  }
}

